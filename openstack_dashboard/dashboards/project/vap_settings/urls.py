from django.conf.urls import patterns
from django.conf.urls import url

from openstack_dashboard.dashboards.project.vap_settings.views \
    import IndexView


urlpatterns = patterns(
    'vap_settings',
    url(r'^$', IndexView.as_view(), name='index'),
)
