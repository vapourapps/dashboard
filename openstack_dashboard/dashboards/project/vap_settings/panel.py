from django.utils.translation import ugettext_lazy as _

import horizon
from openstack_dashboard.dashboards.project import dashboard

class Vap_Settings(horizon.Panel):
    name = _("Vapourapps Settings")
    slug = "vap_settings"


dashboard.Project.register(Vap_Settings)
