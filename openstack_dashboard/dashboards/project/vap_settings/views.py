from django.views.generic import TemplateView
from django.utils.safestring import mark_safe
from django.shortcuts import redirect
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from horizon import forms
from openstack_dashboard.salt_utils import salt_utils
import forms as project_forms



class IndexView(TemplateView):
    template_name = 'project/vap_settings/index.html'

    def post(self, request, *args, **kwargs):
        #TODO read POST data, use it to change settings in salt. 
        return redirect('horizon:project:vap_settings:index')

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        settings_form = project_forms.SettingsFields(self.request)
        context['vap_fields'] = settings_form
        return context

