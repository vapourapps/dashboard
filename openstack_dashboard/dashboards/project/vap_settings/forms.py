from horizon import forms, tables, messages
from django.core.urlresolvers import reverse_lazy
from openstack_dashboard.salt_utils import salt_utils
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import redirect

class SettingsFields(forms.SelfHandlingForm):
    domain_name = forms.CharField(max_length = 50, widget = forms.TextInput(attrs={'size': '40', 'readonly': True}))
    company_name = forms.CharField(max_length = 50, widget = forms.TextInput(attrs={'size': '40'}))
    email_for_notification = forms.CharField(max_length = 50, widget = forms.TextInput(attrs={'size': '40'}))

    def __init__(self, request, *args, **kwargs):
        super(SettingsFields, self).__init__(self, *args, **kwargs)
        domain, company  = salt_utils.get_domain_and_company_name(request)
        self.fields['company_name'].widget.attrs['value'] = company
        self.fields['domain_name'].widget.attrs['value'] = domain

    def handle(self, request, data):
        return redirect('horizon:project:vap_settings:index')
