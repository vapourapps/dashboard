from django.utils.translation import ugettext_lazy as _

import horizon
from openstack_dashboard.dashboards.project import dashboard

class Inventory(horizon.Panel):
    name = _("Inventory")
    slug = "inventory"


dashboard.Project.register(Inventory)
