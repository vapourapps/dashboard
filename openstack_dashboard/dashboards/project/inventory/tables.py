from horizon import tables
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _


[u'id', u'os osarch osmajorrelease osrelease', u'num_cpus cpu_model cpuarch', u'mem_total', u'ip4_interfaces', u'virtual']
class InventoryTable(tables.DataTable):
    host_id = tables.Column('host_id', verbose_name=_("Id"))
    OS = tables.Column('CPU', verbose_name=_("OS"))
    CPU = tables.Column('CPU', verbose_name=_("CPU"))
    mem_total = tables.Column('mem_total', verbose_name=_("Memory total"))
    ip_interfaces = tables.Column('ip_interfaces', verbose_name=_("IP Addresses"))  
    virtual = tables.Column('virtual', verbose_name=_("Virtual"))  

    class Meta:
        name = "vap_inventory"
        verbose_name = _("Inventory")
