from django.views.generic import TemplateView
from django.utils.safestring import mark_safe
from django.shortcuts import redirect
import openstack_dashboard.salt_utils.salt_utils as salt_utils
import openstack_dashboard.salt_utils.utils as salt_api
import json
import tables as project_tables

class InventoryInstance:
    def __init__(self, host_id, OS, CPU, mem_total, ip_interfaces, virtual):
        self.id = self.host_id = host_id
        self.OS = OS
        self.CPU = CPU
        self.mem_total = mem_total
        self.ip_interfaces = ip_interfaces
        self.virtual = virtual

class IndexView(TemplateView):
    template_name = 'project/inventory/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)

        columns =[u'id', u'os osarch osmajorrelease osrelease', u'num_cpus cpu_model cpuarch', u'mem_total', u'ip4_interfaces', u'virtual']
        rows = salt_utils.get_table_rows(self.request, columns)
        print rows
        table = project_tables.InventoryTable(self.request, data = [InventoryInstance(*x) for x in rows])
        context['table'] = table
        return context

