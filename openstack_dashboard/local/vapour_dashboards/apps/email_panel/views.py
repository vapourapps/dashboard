from django.views.generic import TemplateView
from django.utils.safestring import mark_safe
from django.shortcuts import redirect
import openstack_dashboard.salt_utils.salt_utils as salt_utils
import openstack_dashboard.salt_utils.utils as salt_api
import json, tables as project_tables
from collections import namedtuple

class EmailUser(object):
    def __init__(self, username = '', aliases = [], quota_used = '0%', unread = 0, last_access = 0):
        self.id = self.username = username
        self.aliases = ', '.join(aliases)
        self.quota_used = quota_used
        self.unread = unread
        self.last_access = last_access


class IndexView(TemplateView):
    template_name = 'apps/email_panel/index.html'

    def get_tables(self):
        users = salt_utils.get_samba_users(self.request, fields = ['username', 'name'])['users']
        email_table = project_tables.EmailUsers(self.request, [EmailUser(username = u['username']) for u in users])
        return email_table

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['email_table'] = self.get_tables()
        context['panel_header'] = 'E-mail'
        context['panel_title'] = 'E-mail'
        return context
