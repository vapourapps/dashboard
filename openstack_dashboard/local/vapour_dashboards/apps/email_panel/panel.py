from django.utils.translation import ugettext_lazy as _

import horizon
from openstack_dashboard.local.vapour_dashboards.apps import dashboard

class Email_Panel(horizon.Panel):
    name = _("E-mail")
    slug = "email_panel"


dashboard.Dashboard.register(Email_Panel)
