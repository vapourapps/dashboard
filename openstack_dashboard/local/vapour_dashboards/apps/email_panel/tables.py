from horizon import tables
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _

class EmailUsers(tables.DataTable):
    username = tables.Column('username', verbose_name=_("Username"))
    aliases = tables.Column('aliases', verbose_name = _('Aliases'))
    quota_used = tables.Column('quota_used', verbose_name=_("Quota used"))
    unread = tables.Column('unread', verbose_name=_("Unread"))
    last_access = tables.Column('last_access', verbose_name=_("Last access"))
    class Meta:
        name = "Shares"
        verbose_name = _("Shares")
