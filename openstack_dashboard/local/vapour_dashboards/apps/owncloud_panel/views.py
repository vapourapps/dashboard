from django.views.generic import TemplateView
from django.utils.safestring import mark_safe
from django.shortcuts import redirect
import openstack_dashboard.salt_utils.salt_utils as salt_utils
import openstack_dashboard.salt_utils.utils as salt_api
import json
import tables as project_tables


class Share(object):
    def __init__(self, shared_folder = '', users_with_access = '', last_access = ''):
        self.id = shared_folder
        self.shared_folder = shared_folder
        self.users_with_access = users_with_access
        self.last_access = last_access

class OwncloudUser(object):
    def __init__(self, name = '', quota = '', last_access = ''):
        self.id = name
        self.name = name
        self.quota = quota
        self.last_access = last_access

class IndexView(TemplateView):
    template_name = 'apps/owncloud_panel/index.html'
    @salt_utils.timeout_function
    def get_salt_data(self):
        return salt_utils.get_owncloud_users(self.request), salt_utils.get_owncloud_files(self.request)

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        if salt_utils.get_instance_status(self.request, 'va-owncloud') != 'running' : 
            context['users_table'] = context['shares_table'] = None 
            return context

        users, shares = self.get_salt_data()
        users_table = project_tables.Users(self.request, [OwncloudUser(*u) for u in users])  
        shares_table = project_tables.Shares(self.request, [Share(*s) for s in shares])

        context['users_table'] = users_table
        context['shares_table'] = shares_table

        return context
