from django.utils.translation import ugettext_lazy as _

import horizon
from openstack_dashboard.local.vapour_dashboards.apps import dashboard

class Owncloud_Panel(horizon.Panel):
    name = _("Owncloud")
    slug = "owncloud_panel"


dashboard.Dashboard.register(Owncloud_Panel)
