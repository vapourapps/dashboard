from django.views.generic import TemplateView
from django.utils.safestring import mark_safe
from django.shortcuts import redirect
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from horizon import forms, messages
import openstack_dashboard.salt_utils.salt_utils as salt_utils
import json, re, datetime, time, forms as project_forms, tables as project_tables


#Finds the regex in the (rendered) table, constructs the new url for the specified hot, and returns the new table. 
def sub_url(regex, table, replace, host):
    url = re.search(regex, table)
    if url:
        url = str(url.group(1)) + host
        return mark_safe(table.replace(replace, url))
    return table



#Gets called on a url of type /removebackup/host-name/path/to/file. Calls a saltstack function to remove the file from the backup list.
def remove_backup(request, host, backup):
    result = salt_utils.remove_backup(request, host, backup)
    if result: messages.success(request, 'Backup is removed successfuly. ')
    else: messages.error(request, 'Backup wasn\'t removed; try again or contact your administrator. ')
    path = request.get_full_path().replace('removebackup/' + host + '/' + backup, "", 1) 
    return redirect(path)


class AddBackupView(forms.ModalFormView):
    form_class = project_forms.AddBackupForm
    template_name = "apps/backup.manage_backups/new_backup.html"
    success_url = reverse_lazy('horizon:apps:backup.manage_backups:index')

    def get_context_data(self, **kwargs):
        context = super(AddBackupView, self).get_context_data(**kwargs)
        if self.args[0]:
            self.request.session['host'] = self.args[0]
        return context

    def get_form_kwargs(self):
        kwargs = super(AddBackupView, self).get_form_kwargs()
        return kwargs


    def get_initial(self):
        return {}


class KeepBackupsView(forms.ModalFormView):
    form_class = project_forms.KeepBackupsForm
    template_name = "apps/backup.manage_backups/keep_backups.html"
    success_url = reverse_lazy('horizon:apps:backup.manage_backups:index')

    def get_context_data(self, **kwargs):
        context = super(KeepBackupsView, self).get_context_data(**kwargs)
        if self.args[0]:
            self.request.session['host'] = self.args[0]
        return context

    def get_initial(self):
        return {}

class Backup(object):
    def __init__(self, backup_name):
        self.id = backup_name
        self.backup_name = backup_name

class InstancesView(TemplateView):
    template_name = "apps/backup.manage_backups/instances.html"

    @salt_utils.timeout_function
    def get_context_data(self, **kwargs):
        context = super(InstancesView, self).get_context_data(**kwargs)
        if salt_utils.get_instance_status(self.request, 'va-backup') != 'running' : 
            context['backup_hosts'] = None
            return context

        hosts_fqdns = salt_utils.get_host_fqdn_pairs(self.request)
#        warnings was used to show host errors if there were any. Not used right now. 
#        warnings = salt_utils.get_warnings_from_dict(salt_utils.get_icinga2_data(self.request))
        rows = salt_utils.get_backed_up_files(self.request, hosts_fqdns)
        last_backups = salt_utils.get_last_backups(self.request, hosts_fqdns)
        hosts = hosts_fqdns.keys()

        #Fairly smelly code ahead. I wanted to use django-tables for consistency, but they aren't really fit to be used for multiple tables for the same page. 
        #Basically, I render all the tables and then substitute the urls manually because you can't give the value of the host for each table. Long story short, I append the hostname to each url manually. 
        all_hosts = []

        newbackup_regex = "href=\'(.*newbackup/)\'"
        keepbackup_regex = "href=\'(.*keep_.*/)\'"
        deletebackup_regex = "href=\'(.*removebackup/).*\'"
        backup_column_regex = "Backup name:"

        host_info = {host : {'name' : host, 'last_backup' : last_backups.get(host, 'n/a')} for host in hosts}

        for host in hosts:
        
            host_table = project_tables.BackupTable(self.request, [Backup(backup) for backup in rows.get(host)], host_info = host_info[host])

            host_table = host_table.render()
            host_table = sub_url(newbackup_regex, host_table, '/apps/backup/manage_backups/newbackup/', hosts_fqdns[host])
            host_table = sub_url(keepbackup_regex, host_table, '/apps/backup/manage_backups/keep_backups/', hosts_fqdns[host])
            host_table = sub_url(deletebackup_regex, host_table, '/apps/backup/manage_backups/removebackup//', hosts_fqdns[host])
            host_table = mark_safe(host_table.replace('Backup name:', host_info[host]['name'] + ' (last backup : ' + host_info[host]['last_backup'] + ')'))
            all_hosts.append({'table': host_table, 'info' : host_info[host]})

        context['backup_hosts'] = all_hosts

        #If the user tried to add a new backup, but the file doesn't exist, this shows a message. 
        if self.args:
            if 'notfound' in self.args[0]:
                context['notfound'] = self.args[0].split('notfound')[1]
        return context
