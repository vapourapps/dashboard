import horizon
from ... import dashboard

class Panel(horizon.Panel):
    name = "Manage backups"
    slug = "backup.manage_backups"

dashboard.Dashboard.register(Panel)
