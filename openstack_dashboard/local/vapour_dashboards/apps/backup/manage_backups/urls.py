from django.conf.urls import patterns  # noqa
from django.conf.urls import url  # noqa
from . import views

urlpatterns = patterns('apps/backup.manage_backups',
                       url(r'^$', views.InstancesView.as_view(), name='index'),
                       url(r'^(notfound/[/\w]*)?$', views.InstancesView.as_view()),
                       url(r'^newbackup/(.*)', views.AddBackupView.as_view(), name='new_backup'),
                       url(r'^removebackup/([-\./\w]*)/([/\w]*)/?', views.remove_backup, name = 'remove_backup'),
                       url(r'^keep_backups/([-/\w]*)', views.KeepBackupsView.as_view(), name = 'keep_backup'),
                       )
