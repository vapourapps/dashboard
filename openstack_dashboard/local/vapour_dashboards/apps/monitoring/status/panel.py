from django.utils.translation import ugettext_lazy as _

import horizon

from ... import dashboard

class Status(horizon.Panel):
    name = _("Status")
    slug = 'monitoring.status'


dashboard.Dashboard.register(Status)
