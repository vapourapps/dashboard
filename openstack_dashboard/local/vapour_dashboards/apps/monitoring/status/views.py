from django.views.generic import TemplateView
from ...utils import *
from django.utils.safestring import mark_safe
from django.http import JsonResponse
import json

monitoring_minion = 'G@role:monitoring'

class IndexView(TemplateView):
    template_name = 'apps/monitoring.status/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        api = get_salt_api(self.request)
        result = api.call_function_single_target(monitoring_minion, 'monitoring.icinga2')
        context['monitoring_hosts'] = result
        return context

class ChartView(TemplateView):
    template_name = 'apps/monitoring.status/charts.html'

    def get_context_data(self, **kwargs):
        context = super(ChartView, self).get_context_data(**kwargs)
        api = get_salt_api(self.request)
        result = api.call_function_single_target(monitoring_minion,
                                                 'monitoring_stats.parse', {
                    'host': kwargs['host'],
                    'service': kwargs['service']
        })
        context['json_result'] = mark_safe(json.dumps(result))
        context['chart_name'] = '%s - %s' % (kwargs['host'], kwargs['service'])
        return context

def chart_rest(request, **kwargs):
    api = get_salt_api(request)
    result = api.call_function_single_target(monitoring_minion,
                                             'monitoring_stats.parse', {
                'host': kwargs['host'],
                'service': kwargs['service'],
                'start': kwargs['start'],
                'interval': kwargs['interval']
    })
    return JsonResponse(result)
