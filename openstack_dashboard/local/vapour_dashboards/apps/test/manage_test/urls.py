from django.conf.urls import patterns  # noqa
from django.conf.urls import url  # noqa
from . import views

urlpatterns = patterns('',
                       url(r'^$', views.InstancesView.as_view(), name='index'),
                       )
