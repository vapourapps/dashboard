import horizon
from ... import dashboard

class Panel(horizon.Panel):
    name = "Manage test"
    slug = "test.manage_test"
    permissions = ('openstack.roles.admin', 'openstack.services.compute')

dashboard.Dashboard.register(Panel)
