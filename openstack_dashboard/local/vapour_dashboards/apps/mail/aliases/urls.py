from django.conf.urls import patterns  # noqa
from django.conf.urls import url  # noqa
from django.views.generic.base import TemplateView

class IndexView(TemplateView):
    template_name = 'mail/aliases/index.html'


urlpatterns = patterns('', url(r'^$', IndexView.as_view(), name='index'),)
