import horizon
from ... import dashboard

class AliasesPanel(horizon.Panel):
    name = "Aliases"
    slug = "mail.aliases"

dashboard.Dashboard.register(AliasesPanel)
