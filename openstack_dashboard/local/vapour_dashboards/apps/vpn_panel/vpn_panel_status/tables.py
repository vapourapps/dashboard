from horizon import tables
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _


class VPNStatus(tables.DataTable):
    name = tables.Column('name', verbose_name=_("Name"))
    connected_since = tables.Column('connected_since', verbose_name=_("Connected Since"))
    ip_address = tables.Column('ip_address', verbose_name=_("IP address"))
    bytes_in = tables.Column('bytes_in', verbose_name=_("Bytes in"))
    bytes_out = tables.Column('bytes_out', verbose_name=_("Bytes out"))  
    class Meta:
        name = "VPNStatus"
        verbose_name = _("VPN status")
