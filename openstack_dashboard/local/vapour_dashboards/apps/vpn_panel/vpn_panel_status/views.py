from django.views.generic import TemplateView
from django.utils.safestring import mark_safe
from django.shortcuts import redirect
from horizon import tables
import openstack_dashboard.salt_utils.salt_utils as salt_utils
import openstack_dashboard.salt_utils.utils as salt_api
import json, time
import tables as project_tables

class VPNUser(object):
    def __init__(self, name, connected_since, ip_address, bytes_in, bytes_out):
        self.id = name
        self.name = name
        self.connected_since = connected_since
        self.ip_address = ip_address
        self.bytes_in = bytes_in
        self.bytes_out = bytes_out

class IndexView(TemplateView):
    template_name = 'apps/vpn_panel.vpn_panel_status/index.html'

    @salt_utils.timeout_function
    def get_tables(self):
        users = salt_utils.get_vpn_status(self.request)
        users = users.get('client_list') or []
        rows = [VPNUser(user, users[user]['Connected Since'], users[user]['Real Address'], users[user]['Bytes Sent'], users[user]['Bytes Received']) for user in users]
        return rows

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        if salt_utils.get_instance_status(self.request, 'va-directory') != 'running' : 
            context['status'] = None
            return context
        status = project_tables.VPNStatus(self.request, self.get_tables())
        context['status'] = status
        return context
