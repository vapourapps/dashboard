from django.utils.translation import ugettext_lazy as _

import horizon
from openstack_dashboard.local.vapour_dashboards.apps import dashboard

class Vpn_Panel_Status(horizon.Panel):
    name = _("Vpn Status")
    slug = "vpn_panel.vpn_panel_status"


dashboard.Dashboard.register(Vpn_Panel_Status)
