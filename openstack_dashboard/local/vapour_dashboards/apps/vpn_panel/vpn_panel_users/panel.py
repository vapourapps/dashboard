from django.utils.translation import ugettext_lazy as _

import horizon
from openstack_dashboard.local.vapour_dashboards.apps import dashboard

class Vpn_Panel_Users(horizon.Panel):
    name = _("Vpn Users")
    slug = "vpn_panel.vpn_panel_users"


dashboard.Dashboard.register(Vpn_Panel_Users)
