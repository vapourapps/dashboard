from horizon import tables, forms, messages
from django.views.generic import TemplateView
from django.utils.safestring import mark_safe
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse
import openstack_dashboard.salt_utils.salt_utils as salt_utils
import openstack_dashboard.salt_utils.utils as salt_api
import json, forms as project_forms, tables as project_tables

def new_user(request):
    result = salt_utils.add_openvpn_user(request, request.POST['username'])
    if result: 
        messages.success(request, 'Openvpn user added')
    else:
        messages.error(request, 'User wasn\'t created. Try again or contact an administrator. ')
    return redirect(reverse_lazy('horizon:apps:vpn_panel.vpn_panel_users:index'))

def revoke(request, user):
    result = salt_utils.revoke_openvpn_user(request, user)
    if result: 
        messages.success(request, 'User revoked')
    else:
        messages.error(request, 'User wasn\'t revoked. Try again or contact an administrator. ')
    return redirect(reverse_lazy('horizon:apps:vpn_panel.vpn_panel_users:index'))

def send_on_mail(request, user):
    cert = salt_utils.get_user_cert(request, user)
    response = HttpResponse(content_type = 'text/plain')
    response['Content-Disposition'] = 'attachment; filename = ' + user + '.ovpn'
    response.write(cert)
    return response


class User(object):
    def __init__(self, name, connected = False):
        self.id = name
        self.name = name
        self.connected = connected

class Login(object):
    def __init__(self, login_date, ip_address):
        self.id = login_date
        self.login_date = login_date
        self.ip_address = ip_address

class AddUserView(forms.ModalFormView):
    form_class = project_forms.AddVPNUserForm
    template_name = "apps/vpn_panel.vpn_panel_users/templates/add_user.html"
    success_url = reverse_lazy('horizon:apps:vpn_panel.vpn_panel_users:index')

    def get_context_data(self, **kwargs):
        context = super(AddUserView, self).get_context_data(**kwargs)  
        return context

    def get_form_kwargs(self):
        kwargs = super(AddUserView, self).get_form_kwargs()
        samba_users = salt_utils.salt_cache_get('samba_users')['users']
        kwargs['samba_users'] = [x['username'] for x in samba_users if x]
        return kwargs

    def get_initial(self):
        return {}


class IndexView(tables.DataTableView):
    table_class = project_tables.ActiveUsers
    template_name = 'apps/vpn_panel.vpn_panel_users/templates/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        if salt_utils.get_instance_status(self.request, 'va-directory') != 'running' : 
            context['active_users'] = context['revoked_users'] = False
            return context
 
        result = salt_utils.get_openvpn_users(self.request, list_samba_users = True, get_status = True) 
        print 'Result is : ', result
        if result == False: 
            users_with_status = {}
            users_list = {}
        else:    
            users_with_status = {'active' : result['active'], 'revoked' : result['revoked']}
            users_list = result['status'].get('client_list', {})
            self.request.session['samba_users'] = result['samba_users']
            self.request.session.set_test_cookie()

        active_users = users_with_status.get('active', [])
        revoked_users = users_with_status.get('revoked', [])

        active_rows = [User(user, user in users_list.keys()) for user in active_users]
        revoked_rows = [User(user, user in users_list.keys()) for user in revoked_users]


        active = project_tables.ActiveUsers(self.request, active_rows, name = 'Active users')
        revoked = project_tables.RevokedUsers(self.request, revoked_rows, name = 'Revoked users')
        context['active_users'] = active
        context['revoked_users'] = revoked
        return context  

class ListLoginsView(tables.DataTableView):
    table_class = project_tables.ListLogins
    template_name = 'apps/vpn_panel.vpn_panel_users/templates/listlogins.html'

    def get_context_data(self, **kwargs):
        context = super(ListLoginsView, self).get_context_data(**kwargs)
        rows = salt_utils.list_user_logins(self.request, self.args[0])
        list_logins = [Login(row['login_time'], row['ip_address']) for row in rows]

        list_logins_table = project_tables.ListLogins(self.request, list_logins)

        context['user'] = self.args[0]
        context['list_logins_table'] = list_logins_table
        return context  
