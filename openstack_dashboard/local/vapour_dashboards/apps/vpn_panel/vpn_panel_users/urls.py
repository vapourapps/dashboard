from django.core.urlresolvers import reverse_lazy
from django.conf.urls import patterns
from django.conf.urls import url

from openstack_dashboard.local.vapour_dashboards.apps.vpn_panel.vpn_panel_users.views \
    import IndexView

from . import views

urlpatterns = patterns(
    'apps/vpn_panel.vpn_panel_users',
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'send_on_mail/(.*)$', views.send_on_mail, name = 'send_mail'), 
    url(r'list_logins/(.*)$', views.ListLoginsView.as_view(), name = 'list_logins'),
    url(r'revoke/(.*)$', views.revoke, name = 'revoke'),
    url(r'new_user$', views.AddUserView.as_view(), name = 'new_user'),
)
