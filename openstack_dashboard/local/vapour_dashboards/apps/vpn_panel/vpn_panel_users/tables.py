from horizon import tables
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _

class RevokeUser(tables.LinkAction):
    name = "revoke_user"
    verbose_name = _("Revoke user")
    url = "horizon:apps:vpn_panel.vpn_panel_users:revoke"

class ListLogins(tables.LinkAction):
    name = "list_logins"
    verbose_name = _("List logins")
    url = "horizon:apps:vpn_panel.vpn_panel_users:list_logins"

class SendOnMail(tables.LinkAction):
    name = "send_mail"
    verbose_name = _("Download certificate")
    url = "horizon:apps:vpn_panel.vpn_panel_users:send_mail"

class AddUser(tables.LinkAction):
    name = "add_user"
    verbose_name = _("Add user")
    url = "horizon:apps:vpn_panel.vpn_panel_users:new_user"
    classes = ('ajax-modal', 'btn-create')
    icon = 'plus'
    ajax = True


class ActiveUsers(tables.DataTable):
    name = tables.Column('name', verbose_name=_("Name"))
    connected = tables.Column('connected', verbose_name = _('Connected'))
    class Meta:
        name = "active_users"
        verbose_name = _("Active Users")
        table_actions = (AddUser, )
        row_actions = (SendOnMail, RevokeUser, ListLogins, )


class RevokedUsers(tables.DataTable):
    name = tables.Column('name', verbose_name=_("Name"))
    connected = tables.Column('connected', verbose_name = _('Connected'))
    class Meta:
        name = "revoked_users"
        verbose_name = _("Revoked Users")


class ListLogins(tables.DataTable):
    login_date = tables.Column('login_date', verbose_name = _('Login date'))
    ip_address = tables.Column('ip_address', verbose_name = _('IP address'))

    class Meta:
        name = "list_logins"
        verbose_name = _("User logins")

