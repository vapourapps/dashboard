from horizon import forms, messages
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.core import exceptions
from openstack_dashboard.salt_utils import salt_utils

directory_minion = 'G@role:directory'

class AddVPNUserForm(forms.SelfHandlingForm):
    username = forms.RegexField(label=_("Username"), max_length=32, regex=r'[^\n]+$', required = False)
    list_users = forms.ChoiceField(choices=[(1, None)], label = _("Choose an existing user"),  required = False)

    def __init__(self, request, *args, **kwargs):
        users = kwargs.pop('samba_users')
        super(AddVPNUserForm, self).__init__(args, kwargs)
        self.fields['list_users'].choices += [(x, x) for x in users]

    def clean(self):
        super(AddVPNUserForm, self).clean()
        if self.data.get('data') :
            cleaned_data = {'username' : self.data.get('data').get('username'), 'list_users' : self.data.get('data').get('list_users')}
            if cleaned_data.get('username') or cleaned_data.get('list_users'):
                return cleaned_data   
        return False

    def handle(self, request, data):
        if data['list_users']: username = data['list_users']
        else: username = data['username']
        result = salt_utils.add_openvpn_user(request, username)
        if result: 
            messages.success(request, 'User added successfuly. ')
            return redirect('horizon:apps:vpn_panel.vpn_panel_users:index')
        else: messages.error(request, 'User wasn\'t added; Try again or contact an administrator. ')
