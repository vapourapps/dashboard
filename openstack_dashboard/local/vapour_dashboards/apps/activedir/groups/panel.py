import horizon
from ... import dashboard

class GroupsPanel(horizon.Panel):
    name = "Groups"
    slug = "activedir.groups"

dashboard.Dashboard.register(GroupsPanel)
