from horizon import forms, messages
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.utils.translation import ugettext_lazy as _
from ...utils import *
from openstack_dashboard.salt_utils import salt_utils

directory_minion = 'G@role:directory'

class AddUserForm(forms.SelfHandlingForm):
    group_name = forms.RegexField(label=_("Group name"), max_length=32, regex=r'[^\n]+$')

    def handle(self, request, data):
        result = salt_utils.new_group(request, data['group_name'])
        if result: 
            messages.success(request, 'Group added successfuly. ')
            return True
        messages.error(request, 'Group wasn\'t added. Try again or contact an administrator. ')


class AddUserToGroupForm(forms.SelfHandlingForm):
    list_users = forms.MultipleChoiceField(choices=[(1, 'n/a')], label = _("Users not in group"), widget=forms.CheckboxSelectMultiple)

    def __init__(self, request, *args, **kwargs):
        users = kwargs.pop('samba_users')
        super(AddUserToGroupForm, self).__init__(self, *args, **kwargs)
        self.fields['list_users'].choices = [(x['username'], x['username']) for x in users]

    def handle(self, request, data):
        result = salt_utils.add_user_to_group(request, data['list_users'], request.session['group_name'])
        if result: 
            messages.success(request, 'Users added successfuly. ')
            return True
        messages.error(request, 'Users weren\'t added. Try again or contact an administrator. ')
