from horizon import tables, forms, messages
from django.core.urlresolvers import reverse_lazy, reverse
from django.shortcuts import redirect, render
from django.utils.translation import ugettext_lazy as _
from ...utils import *
from . import tables as project_tables
from . import forms as project_forms
from openstack_dashboard.salt_utils import salt_utils
import inspect

directory_minion='G@role:directory'

@salt_utils.timeout_function
def RemoveUserFromGroup(request, user):
    group_name = request.session['group_name']
    result = salt_utils.remove_user_from_group(request, user, group_name)
    if result: 
        messages.success(request, 'User removed from group ')
    else:
        messages.error(request, 'User wasn\'t removed from group. Try again or contact an administrator. ')
    return redirect(reverse('horizon:apps:activedir.groups:view_members', args = [group_name]))

@salt_utils.timeout_function
def DeleteGroup(request, group_name):
    result = salt_utils.delete_group(request, group_name)
    if result: 
        messages.success(request, 'Group deleted successfuly. ')
    else :
        messages.error(request, 'Group wasn\'t deleted. Try again or contact an administrator. ')
    return redirect(reverse('horizon:apps:activedir.groups:index'))

class Group(object):
    def __init__(self, name):
        self.id = name
        self.name = name

class GroupMember(object):
    def __init__(self, username, join_date):
        self.id = username
        self.username = username
        self.join_date = join_date


#Because we're using django forms and tables, I had to work around some stuff
#When the page is loaded, the session saves the name of the group (otherwise inaccessible by the form and we need it there)
#The users in the group as well as all samba users not in the group are also saved in the session
#The AddUserToGroupView takes these users from the session and passes them to the form
class GroupMembersView(tables.DataTableView):
    table_class = project_tables.GroupMembersTable
    template_name = 'apps/activedir.groups/members.html'

    def get_data(self):
        api = get_salt_api(self.request)
        result = api.call_function_single_target(directory_minion, 'samba.list_group_members', {'group_name': self.kwargs['name']})
        samba_users = salt_utils.get_samba_users(self.request)['users']
        self.request.session['group_name'] = self.kwargs['name']
        self.request.session['group_users'] = result
        self.request.session['samba_users'] = [user for user in samba_users if user['username'] not in self.request.session['group_users']]
        return [GroupMember(username, '') for username in result if '.ldb' not in username]


class AddGroupView(forms.ModalFormView):
    form_class = project_forms.AddUserForm
    template_name = "apps/activedir.groups/add.html"
    success_url = reverse_lazy('horizon:apps:activedir.groups:index')

    def get_context_data(self, **kwargs):
        context = super(AddGroupView, self).get_context_data(**kwargs)
        return context

    def get_initial(self):
        return {}

class AddUserToGroupView(forms.ModalFormView):
    form_class = project_forms.AddUserToGroupForm
    template_name = "apps/activedir.groups/add_user_to_group.html"
    success_url = ''

    def get_context_data(self, **kwargs):
        self.success_url = reverse_lazy('horizon:apps:activedir.groups:view_members', args = [self.request.session['group_name']])
        context = super(AddUserToGroupView, self).get_context_data(**kwargs)
        return context

    def get_success_url(self):
        return self.success_url

    def get_form_kwargs(self):
        kwargs = super(AddUserToGroupView, self).get_form_kwargs()
        kwargs['samba_users'] = self.request.session['samba_users']
        return kwargs

    def get_initial(self):
        return {}



class IndexView(tables.DataTableView):
    table_class = project_tables.GroupTable
    template_name = 'apps/activedir.groups/groups.html'

    def get_data(self):
        if salt_utils.get_instance_status(self.request, 'va-directory') != 'running' :
            return [] 

        api = get_salt_api(self.request)
        result = api.call_function_single_target(directory_minion, 'samba.list_groups')
        groups = []
        for group in result:
            if group not in salt_utils.samba_secondary_groups:
                groups.append(Group(name=group))
        return groups

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        secondary = project_tables.GroupTable(self.request, [Group(name = x) for x in salt_utils.samba_secondary_groups])
        context['secondary_groups'] = secondary
        return context  
