from django.conf.urls import patterns  # noqa
from django.conf.urls import url  # noqa

from . import views

urlpatterns = patterns('', url(r'^$', views.IndexView.as_view(), name='index'),
                           url(r'^add$', views.AddView.as_view(), name='add'),
                           url(r'^edit$', views.EditView.as_view(), name='edit'),
                      )

