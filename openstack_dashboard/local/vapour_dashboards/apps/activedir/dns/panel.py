import horizon
from ... import dashboard

class DNS(horizon.Panel):
    name = "DNS entries                                                 "
    slug = "activedir.dns"

dashboard.Dashboard.register(DNS)
