from django.conf.urls import patterns  # noqa
from django.conf.urls import url  # noqa

from . import views, modal_views

urlpatterns = patterns('', url(r'^$', views.IndexView.as_view(), name='index'),
                           url(r'^add$', views.AddUserView.as_view(), name='add'),
                           url(r'^(?P<username>[^/]+)/change_name$', modal_views.ChangeNameView.as_view(), name='change_name'),
                           url(r'^(?P<username>[^/]+)/change_password$', modal_views.ChangePasswordView.as_view(), name='change_password'),
                           url(r'^(?P<username>[^/]+)/toggle_lock$', modal_views.ToggleLockView.as_view(), name='toggle_lock'),
                           url(r'^(?P<username>[^/]+)/list_logins$', modal_views.ListLoginsView.as_view(), name='list_logins'),
                           url(r'^(?P<username>[^/]+)/manage_groups$', modal_views.ManageGroupsView.as_view(), name='manage_groups'),
                           url(r'^sync_cache$', views.sync_cache, name='sync_cache'),)
