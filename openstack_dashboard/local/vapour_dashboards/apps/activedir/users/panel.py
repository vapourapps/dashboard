import horizon
from ... import dashboard

class UserPanel(horizon.Panel):
    name = "View/Add users"
    slug = "activedir.users"

dashboard.Dashboard.register(UserPanel)
