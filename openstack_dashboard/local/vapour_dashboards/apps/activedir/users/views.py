from horizon import tables, forms, messages
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect
from django.utils.translation import ugettext_lazy as _
from openstack_dashboard.salt_utils.utils import *
from openstack_dashboard.salt_utils import salt_utils
from . import forms as project_forms
from . import tables as project_tables

directory_minion = 'G@role:directory'

def sync_cache(request):
    salt_utils.sync_cache(request)
    return redirect('horizon:apps:activedir.users:index')

class IndexView(tables.DataTableView):
    table_class = project_tables.UsersTable
    template_name = 'apps/activedir.users/table.html'
    filtered_users = ['krbtgt', 'dnsquery', 'query']

    def get_data(self):
        if salt_utils.get_instance_status(self.request, 'va-directory') != 'running' :
            return [] 
        result = salt_utils.get_samba_users(self.request, get_groups = True, get_samba_log = True)
        
        self.request.session.set_test_cookie()

        users_result = result['users']
        groups = result['groups']
        if groups : 
            self.request.session['samba_groups_with_users'] = groups
            print 'Setting groups : ', self.request.session
            
        samba_log = result['samba_log']
        users = []
        for user in users_result:
            if user['username'] in self.filtered_users : 
                continue
            log_user = [x for x in samba_log if x['info']['username'] == user['username']]
            if log_user : log_user = log_user[0]
            else : log_user = {'date' : '','info': { 'computer' : '', 'address' : ''}}
            new_user = User(user["username"], user["name"], user["is_locked"],
                              user["password_expiry"], log_user["date"],
                              log_user['info']["computer"], log_user['info']["address"])
            users.append(new_user)
        return users

    def get_context(self):
        context = super(IndexView, self).get_context()

        table = context['table']
        context['table'].paginate(page = request.GET.get('page', 1), per_page = 25)
        return context

class AddUserView(forms.ModalFormView):
    form_class = project_forms.AddUserForm
    template_name = "apps/activedir.users/add.html"
    success_url = reverse_lazy('horizon:apps:activedir.users:index')

    def get_context_data(self, **kwargs):
        context = super(AddUserView, self).get_context_data(**kwargs)
        return context

    def get_initial(self):
        return {}
