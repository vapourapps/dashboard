from horizon import forms, messages
from django.core.urlresolvers import reverse_lazy
from django.core import exceptions
from django.utils.translation import ugettext_lazy as _
from ...utils import *
from openstack_dashboard.salt_utils import salt_utils

directory_minion = 'G@role:directory'

class AddUserForm(forms.SelfHandlingForm):
    username = forms.RegexField(label=_("Username"), max_length=32, regex=r'[^\n]+$')
    password = forms.CharField(widget=forms.PasswordInput)
    name = forms.CharField(label=_("Name"), max_length=64)
    surname = forms.CharField(label=_("Surname"), max_length=64)
    vpn = forms.BooleanField(label=_("Use VPN"), initial=False, required = False)

    def handle(self, request, data):
        result = salt_utils.add_activedir_user(request, data)
        if result:
            messages.success(request, _("Added new user successfully!"))
            return True
        else:
            messages.error(request, _("Failed adding user:%s" % result))
            return False
