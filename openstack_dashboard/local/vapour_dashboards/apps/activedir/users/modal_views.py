from horizon import forms, tables, messages
from django.core.urlresolvers import reverse_lazy
from ...utils import *
from openstack_dashboard.salt_utils import salt_utils
from django.utils.translation import ugettext_lazy as _

directory_minion = 'G@role:directory'

## Change name ##

class ChangeNameForm(forms.SelfHandlingForm):
    username = forms.CharField(widget=forms.HiddenInput())
    name = forms.RegexField(label="Name", max_length=64, regex=r'[a-zA-Z]+')
    surname = forms.RegexField(label="Surname", max_length=64, regex=r'[a-zA-Z]+', required = False)

    def handle(self, request, data):
        name = "%s %s" % (data['name'], data['surname'])
        api = get_salt_api(self.request)
        result = api.call_function_single_target(directory_minion, 'samba.change_name', {
            'username': data['username'],
            'new_name': name
        })

        return result is None

class ChangeNameView(forms.ModalFormView):
    form_class = ChangeNameForm
    template_name = "apps/activedir.users/changename.html"
    success_url = reverse_lazy('horizon:apps:activedir.users:index')

    def get_context_data(self, **kwargs):
        context = super(ChangeNameView, self).get_context_data(**kwargs)
        context["username"] = self.kwargs["username"]
        return context

    def get_initial(self):
        return {"username": self.kwargs["username"]}



## Change password ##

class ChangePasswordForm(forms.SelfHandlingForm):
    username = forms.CharField(widget=forms.HiddenInput())
    password = forms.RegexField(label="New password", max_length=64, regex=r'[^;]{7,}', widget = forms.PasswordInput)
    changeTwice = forms.BooleanField(label="User must change his password the next time he logs in", required=False)
    def handle(self, request, data):
        result = salt_utils.change_password(request, data['username'], data['password'], data['changeTwice'])
        if not result :
            return True
        messages.error(self.request, "Error changing password! : ", result)
        return False

class ChangePasswordView(forms.ModalFormView):
    form_class = ChangePasswordForm
    template_name = "apps/activedir.users/changepassword.html"
    success_url = reverse_lazy('horizon:apps:activedir.users:index')

    def get_context_data(self, **kwargs):
        context = super(ChangePasswordView, self).get_context_data(**kwargs)
        context["username"] = self.kwargs["username"]
        return context

    def get_initial(self):
        return {"username": self.kwargs["username"]}



## Toggle lock ##

class ToggleLockView(forms.ModalFormView):
    form_class = ChangeNameForm
    template_name = "apps/activedir.users/add.html"
    success_url = reverse_lazy('horizon:apps:activedir.users:index')

    def get_context_data(self, **kwargs):
        context = super(ChangePasswordView, self).get_context_data(**kwargs)
        return context

    def get_initial(self):
        return {}


## List logins ##
class LoginsTable(tables.DataTable):
    date = tables.Column('date', verbose_name="Date")
    computer = tables.Column('computer', verbose_name="Computer")
    address= tables.Column('address', verbose_name="IP address")

    class Meta:
        name = "loginstable"
        verbose_name = ("Logins for user")

class ListLoginsView(tables.DataTableView):
    table_class = LoginsTable
    template_name = 'apps/activedir.users/listlogins.html'

    class UserLogin():
        def __init__(self, date, computer, address):
            self.id = self.date = date
            self.computer = computer
            self.address = address


    def get_context_data(self, **kwargs):
        context = super(ListLoginsView, self).get_context_data(**kwargs)
        return context

    def get_data(self):
        samba_log = salt_utils.get_samba_log(self.request)
        user_logins = [x for x in samba_log if x['info']['username'] == self.kwargs['username'] and x['info']['action'] == 'connect' and x['info']['success'] == 'ok' and x['info']['folder'] == 'sysvol']
        if len(user_logins) > 20 : user_logins = user_logins[-19:]
        return [self.UserLogin(x['date'], x['info']['computer'], x['info']['address']) for x in user_logins]
 
    def get_initial(self):
        return {}


class ManageGroups(forms.SelfHandlingForm):
    add_to_groups = forms.MultipleChoiceField(choices=[(1, 'n/a')], label = _("Add user to these groups"), widget=forms.CheckboxSelectMultiple, required = False)
    remove_from_groups = forms.MultipleChoiceField(choices=[(1, 'n/a')], label = _("Remove user from these groups"), widget=forms.CheckboxSelectMultiple, required = False)
    user = ''

    def __init__(self, request, *args, **kwargs):
        user_in_groups = kwargs.pop('user_in_groups')
        user_not_in_groups = kwargs.pop('user_not_in_groups')
        self.user = kwargs.pop('user')
        super(ManageGroups, self).__init__(self, *args, **kwargs)

        self.fields['add_to_groups'].choices = [(x, x) for x in user_not_in_groups]
        self.fields['remove_from_groups'].choices = [(x, x) for x in user_in_groups]

    def handle(self, request, data):
        salt_utils.manage_user_groups(request, self.user, data['add_to_groups'], data['remove_from_groups'])
        return True


class ManageGroupsView(forms.ModalFormView):
    form_class = ManageGroups
    template_name = "apps/activedir.users/managegroups.html"
    success_url = reverse_lazy('horizon:apps:activedir.users:index')


    def get_context_data(self, **kwargs):
        context = super(ManageGroupsView, self).get_context_data(**kwargs)
        context["username"] = self.kwargs["username"]
        return context

#    def get_initial(self):
#        return {"username": self.kwargs["username"]}

    def get_form_kwargs(self):
        kwargs = super(ManageGroupsView, self).get_form_kwargs()
        user = self.kwargs['username']
        groups = salt_utils.salt_cache_get('samba_users')['groups']
        #groups is a dictionary of all groups with their members
        #in this view, we only need the primary groups, so we remake the dictionary to only include them
        groups = {group:groups[group] for group in groups if group not in salt_utils.samba_secondary_groups}
        #... and then separate the groups by whether the user is in them or not
        kwargs['user_in_groups'] = [group for group in groups if user in groups[group]]
        kwargs['user_not_in_groups'] = [group for group in groups if user not in groups[group]]
        kwargs['user'] = user
        return kwargs

