from horizon import tables, messages
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from openstack_dashboard.salt_utils.utils import *
from openstack_dashboard.salt_utils import salt_utils
directory_minion = 'G@role:directory'

#
# Table Actions
#


class FilterUsers(tables.FilterAction):
    name = "filterUsers"
    def filter(self, table, users, filter_string):
        q = filter_string.lower()

        def comp(flavor):
            return q in users.name.lower()

        return filter(comp, users)

class AddUser(tables.LinkAction):
    name = "add_user"
    verbose_name = _("Add user")
    url = "horizon:apps:activedir.users:add"
    classes = ('ajax-modal', 'btn-create')
    icon = 'plus'
    ajax = True

class DeleteUser(tables.DeleteAction):
    name = "deleterule"

    def action_present(self, count):
        return _("Delete users") if count > 1 else _("Delete user")

    def action_past(self, count):
        return _("Deleted users") if count > 1 else _("Deleted user")

    def delete(self, request, obj_id):
        result = salt_utils.delete_activedir_user(request, obj_id)
        result = salt_utils.revoke_openvpn_user(request, obj_id)
        return result

class ChangeName(tables.LinkAction):
    name = "change_name"
    verbose_name = _("Change name")
    url = "horizon:apps:activedir.users:change_name"
    classes = ("ajax-modal", "btn-edit")

class ChangePassword(tables.LinkAction):
    name = "change_password"
    verbose_name = _("Change password")
    url = "horizon:apps:activedir.users:change_password"
    classes = ("ajax-modal", "btn-edit")

class LockUser(tables.DeleteAction):
    name = "lock_user"

    def action_present(self, count):
        return _("Lock users") if count > 1 else _("Lock user")

    def action_past(self, count):
        return _("Locked users") if count > 1 else _("Locked user")

    def delete(self, request, obj_id):
        api = get_salt_api(request)
        result = api.call_function_single_target(directory_minion, 'samba.set_user_status', {
            'lock': True,
            'username': obj_id
        })
        return result is None

class UnlockUser(tables.DeleteAction):
    name = "unlock_user"
    verbose_name = _("Unlock")

    def action_present(self, count):
        return _("Unlock users") if count > 1 else _("Unlock user")

    def action_past(self, count):
        return _("Unlocked users") if count > 1 else _("Unlocked user")

    def delete(self, request, obj_id):
        api = get_salt_api(request)
        result = api.call_function_single_target(directory_minion, 'samba.set_user_status', {
            'lock': False,
            'username': obj_id
        })
        return result is None

class ListLogins(tables.LinkAction):
    name = "list_logins"
    verbose_name = _("List logins")
    url = "horizon:apps:activedir.users:list_logins"
    classes = ("btn-edit", )


class ManageGroups(tables.LinkAction):
    name = "manage_groups"
    verbose_name = _("Manage groups")
    url = "horizon:apps:activedir.users:manage_groups"
    classes = ("ajax-modal", "btn-edit")

class SyncCache(tables.LinkAction):
    name = "sync_cache"
    verbose_name = _("Synchronize cache")
    url = "horizon:apps:activedir.users:sync_cache"

#
# Actual tables
#


class UsersTable(tables.DataTable):
    username = tables.Column('username', verbose_name=_("Username"),
                             link='horizon:apps:activedir.users:list_logins')
    name = tables.Column('name', verbose_name=_("Name"))
    status = tables.Column('status', verbose_name=_("Status"))
    expiry = tables.Column('expiry', verbose_name=_("Days since password change"))
    lastLogon = tables.Column('lastLogon', verbose_name=_("Last login"))
    computer = tables.Column('computer', verbose_name=_("Computer"))
    ip = tables.Column('ip', verbose_name=_("IP address"))
    status = tables.Column('status', status=True, status_choices=(
                           (_("OK"), True), (_("User locked"), False), (_("Password expiry soon"), False), (_("Password is expired"), False)
                           )
    )

    def get_object_display(self, object):
        return object.username

    class Meta:
        name = "usertable"
        verbose_name = _("Users")
        table_actions = (FilterUsers, AddUser, SyncCache)
        row_actions = (DeleteUser, ChangeName, ChangePassword, LockUser, UnlockUser, ListLogins, ManageGroups)
        status_columns = ("status", )

