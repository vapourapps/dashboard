from django.utils.translation import ugettext_lazy as _
from openstack_dashboard.salt_utils import salt_utils
import horizon

class ActiveDir(horizon.PanelGroup):
    slug = "activedir"
    name = _("Active Directory")
    panels = ('activedir.users',
              'activedir.groups',
              'activedir.dns',)

class Backup(horizon.PanelGroup):
    slug = "backup"
    name = _("Backup")
    panels = ('backup.manage_backups',)

#Email panel not currently used. 
#To use, just uncomment this class and add it to dashboard.panels
class EMail(horizon.PanelGroup):
    slug = "email"
    name = _("E-Mail")
    panels = ('email_panel',)

class FileShare(horizon.PanelGroup):
    slug = "fileshare"
    name = _("Fileshare")
    panels = ('fileshare',)

class Owncloud(horizon.PanelGroup):
    slug = "owncloud"
    name = _("Owncloud")
    panels = ('owncloud_panel',)

class VPN(horizon.PanelGroup):
    slug = "vpn_panel"
    name = _("VPN")
    panels = ('vpn_panel.vpn_panel_users', 'vpn_panel.vpn_panel_status',)

class Monitoring(horizon.PanelGroup):
    slug = "monitoring"
    name = _("Monitoring")
    panels = ('monitoring.status',)


class Dashboard(horizon.Dashboard):
    name = "Apps"
    slug = "apps"
    default_panel = "monitoring.status"
    panels = (Monitoring, ActiveDir, Backup, Owncloud, VPN, EMail, FileShare)

horizon.register(Dashboard)
