from horizon import tables
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _

class SharedFolders(tables.DataTable):
    folders = tables.Column('folder_name', verbose_name=_("Folder"))
    used_space = tables.Column('used_space', verbose_name = _('Used space'))
    class Meta:
        name = "Filesharing"
        verbose_name = _("Filesharing")

