from django.utils.translation import ugettext_lazy as _

import horizon
from openstack_dashboard.local.vapour_dashboards.apps import dashboard

class Fileshare(horizon.Panel):
    name = _("Fileshare")
    slug = "fileshare"


dashboard.Dashboard.register(Fileshare)
