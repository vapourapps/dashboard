from horizon import views
from openstack_dashboard.api import nova


class IndexView(views.APIView):
    # A very simple class-based view...
    template_name = 'apps_help/vpn_panel/index.html'

    def get_data(self, request, context, *args, **kwargs):
        instances_list, more = nova.server_list(self.request)
        if 'va-directory' in [x.name for x in instances_list]: 
            owncloud_instance = [x for x in instances_list if x.name == 'va-directory'][0]
            ip_type = owncloud_instance.addresses.keys()[0]
            context['directory_ip'] = owncloud_instance.addresses.get(ip_type)[0].get('addr')

        return context
