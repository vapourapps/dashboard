from django.utils.translation import ugettext_lazy as _

import horizon
from openstack_dashboard.local.vapour_dashboards.apps_help import dashboard

class Vpn_Panel(horizon.Panel):
    name = _("VPN")
    slug = "vpn_panel"


dashboard.Apps_Help.register(Vpn_Panel)
