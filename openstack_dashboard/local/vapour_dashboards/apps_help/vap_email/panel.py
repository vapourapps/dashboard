from django.utils.translation import ugettext_lazy as _

import horizon
from openstack_dashboard.local.vapour_dashboards.apps_help import dashboard


class Vap_Email(horizon.Panel):
    name = _("Email")
    slug = "vap_email"

dashboard.Apps_Help.register(Vap_Email)
