from django.utils.translation import ugettext_lazy as _

import horizon
from openstack_dashboard.local.vapour_dashboards.apps_help import dashboard

class Storage_Panel(horizon.Panel):
    name = _("Storage")
    slug = "storage_panel"


dashboard.Apps_Help.register(Storage_Panel)
