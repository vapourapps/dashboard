from django.utils.translation import ugettext_lazy as _

import horizon
from openstack_dashboard.local.vapour_dashboards.apps_help import dashboard


class Fileshare(horizon.Panel):
    name = _("Fileshare")
    slug = "fileshare"

dashboard.Apps_Help.register(Fileshare)
