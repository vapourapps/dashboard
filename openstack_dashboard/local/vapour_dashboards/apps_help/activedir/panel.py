from django.utils.translation import ugettext_lazy as _

import horizon
from openstack_dashboard.local.vapour_dashboards.apps_help import dashboard

class Activedir(horizon.Panel):
    name = _("Active Directory")
    slug = "activedir"


dashboard.Apps_Help.register(Activedir)
