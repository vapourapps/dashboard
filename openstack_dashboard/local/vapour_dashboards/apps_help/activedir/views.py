from horizon import views
from openstack_dashboard.api import nova

class IndexView(views.APIView):
    # A very simple class-based view...
    template_name = 'apps_help/activedir/index.html'

    def get_data(self, request, context, *args, **kwargs):
        instances_list, more = nova.server_list(self.request)
        if 'va-directory' in [x.name for x in instances_list]: 
            directory_instance = [x for x in instances_list if x.name == 'va-directory'][0]
            print directory_instance
            ip_type = 'private'
            if 'vmnet' in directory_instance.addresses:
                ip_type = 'vmnet'
            context['directory_ip'] = directory_instance.addresses.get(ip_type)[0].get('addr')

        # Add data to the context here...
        return context

