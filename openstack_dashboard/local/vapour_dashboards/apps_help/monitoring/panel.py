from django.utils.translation import ugettext_lazy as _

import horizon
from openstack_dashboard.local.vapour_dashboards.apps_help import dashboard

class Monitoring(horizon.Panel):
    name = _("Monitoring")
    slug = "monitoring"


dashboard.Apps_Help.register(Monitoring)
