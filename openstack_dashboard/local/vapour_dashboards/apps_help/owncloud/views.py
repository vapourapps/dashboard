from horizon import views
from openstack_dashboard.api import nova

class IndexView(views.APIView):
    # A very simple class-based view...
    template_name = 'apps_help/owncloud/index.html'

    def get_data(self, request, context, *args, **kwargs):
        instances_list, more = nova.server_list(self.request)
        if 'va-owncloud' in [x.name for x in instances_list]: 
            owncloud_instance = [x for x in instances_list if x.name == 'va-owncloud'][0]
            print owncloud_instance
            ip_type = 'private'
            if 'vmnet' in owncloud_instance.addresses:
                ip_type = 'vmnet'
            context['owncloud_ip'] = owncloud_instance.addresses.get(ip_type)[0].get('addr')

        # Add data to the context here...
        return context
