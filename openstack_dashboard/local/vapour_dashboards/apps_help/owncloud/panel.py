from django.utils.translation import ugettext_lazy as _

import horizon
from openstack_dashboard.local.vapour_dashboards.apps_help import dashboard

class Owncloud(horizon.Panel):
    name = _("Owncloud")
    slug = "owncloud"


dashboard.Apps_Help.register(Owncloud)
