from horizon import views
from openstack_dashboard.api import nova

class IndexView(views.APIView):
    # A very simple class-based view...
    template_name = 'apps_help/backup/index.html'


    def get_data(self, request, context, *args, **kwargs):
        instances_list, more = nova.server_list(self.request)
        if 'va-backup' in [x.name for x in instances_list]: 
            backuppc_instance = [x for x in instances_list if x.name == 'va-backup'][0]
            print backuppc_instance.addresses
            ip_type = 'private'
            if 'vmnet' in backuppc_instance.addresses:
                ip_type = 'vmnet'
            context['backuppc_ip'] = backuppc_instance.addresses.get(ip_type)[0].get('addr')

        # Add data to the context here...
        return context
