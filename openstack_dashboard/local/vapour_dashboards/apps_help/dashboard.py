from django.utils.translation import ugettext_lazy as _

import horizon
class Apps_Help(horizon.Dashboard):
    name = _("Apps Help")
    slug = "apps_help"
    panels = ( 'monitoring', 'activedir', 'backup', 'owncloud', 'vpn_panel', 'fileshare', 'vap_email', )    
    default_panel = 'monitoring'



horizon.register(Apps_Help)
