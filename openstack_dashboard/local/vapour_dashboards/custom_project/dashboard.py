import horizon

class Dashboard(horizon.Dashboard):
    name = 'custom_project'
    slug = 'custom_project'

horizon.register(Dashboard)
