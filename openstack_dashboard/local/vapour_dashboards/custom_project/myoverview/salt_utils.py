from openstack_dashboard import api as openstack_api
import novaclient, utils, json
from collections import Counter
monitoring_minion = 'G@role:monitoring'
#Gets all host data from monitoring. May need to be changed to allow from multiple hosts.
#Data returned is a dictionary. 
def get_icinga2_data(request):
    api = utils.get_salt_api(request)
    result = api.call_function_single_target(monitoring_minion, 'monitoring.icinga2')
    return result

def get_warnings_from_dict(data, host_name = ''):
    status_list = {x['host_name'] : Counter([y['state'] for y in x['services']]) for x in data}
    if host_name == '':
        return status_list
    else:
        return status_list[host_name]
