from openstack_dashboard import api as openstack_api
import datetime
from . import salt_api

class User:
    def __init__(self, username, name, locked, expiry, lastLogon, computer, ip):
        try:
            passwordDelta = expiry - datetime.datetime.now()
        except Exception:
            passwordDelta = None
        self.id = username
        self.username = username
        self.name = name
        self.status = 'OK'
        if passwordDelta is not None:
            self.expiry = str(passwordDelta.days) + ' days'
            if passwordDelta.days <= 3:
                self.status = 'Password expiry soon' if passwordDelta.days > 1 else 'Password is expired'
        else:
            self.expiry = 'Never expires'
        if locked:
            self.status = 'User locked'
        self.lastLogon = lastLogon
        self.computer = computer
        self.ip = ip

def get_instance_ip(req, instance_name):
    # get all instances for this tenant
    instances, more = openstack_api.nova.server_list(req)
    # Find all instances with that name
    wanted_instances = [x for x in instances if x.name == instance_name]
    if len(wanted_instances) == 0:
        return None  # We didn't find any instance with that name...

    instance = wanted_instances[0]  # Always get the first instance with that name (TODO: Better filtering)
    ip = None
    # Loops through all IPs
    for ip_name, ip_data in instance.addresses.items():
        for ip_props in ip_data:
            if 'addr' in ip_props and ip_props['addr'] != '':
                ip = ip_props['addr']
                break
            else:
                continue
    return ip

SALT_USER = 'admin'
SALT_PASSWORD = 'MLENnDVwAXq3oDn?2'
SALT_INSTANCE_NAME = 'monitoring'

def get_salt_api(request):
    api_ip = get_instance_ip(request, SALT_INSTANCE_NAME)
    if api_ip is None:
        raise RuntimeError('Salt instance(%s) does not exist for current tenant.' % (SALT_INSTANCE_NAME))
    api = salt_api.SaltAPI('http://%s:8000' % api_ip)
    api.login('admin', request.session['token'].id)
    return api
