# Copyright 2012 United States Government as represented by the
# Administrator of the National Aeronautics and Space Administration.
# All Rights Reserved.
#
# Copyright 2012 Nebula, Inc.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import datetime

from django.template.defaultfilters import capfirst  # noqa
from django.template.defaultfilters import floatformat  # noqa
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages

from openstack_dashboard import api as openstack_api
import openstack_dashboard.salt_utils.salt_utils as salt_utils

import novaclient, utils
from horizon.utils import csvbase
from horizon import views
from . import usage
from ...apps.dashboard import Dashboard as apps_Dashboard


#Gets called when an enable/disable button is pressed. 
def instance_toggled(request, instance, action):
    print "I'm triggered!", instance, action
    instances_list, more = openstack_api.nova.server_list(request)
    instance_name = instance.lower()
    print 'Servers are : ', [x.name for x in instances_list]
    server = [x for x in instances_list if instance_name.lower() in x.name]
    if server:
        if action == 'restart':
            openstack_api.nova.server_reboot(request, server[0].id)
        elif action == 'suspend':
            openstack_api.nova.server_suspend(request, server[0].id)
        elif action == 'resume':
            openstack_api.nova.server_resume(request, server[0].id)
    if action == 'enable':
        messages.info(request, 'The instance is going to be created asynchronously. This may take a few minutes. ')
        salt_utils.enable_instance(request, instance_name)

    else:
        print 'Unknown command. Did you manually go to the url?'
    path = request.get_full_path().replace(instance + '/' + action, "", 1)
    return redirect(path)

#This is just a class to store data about the instances. Maybe making it a model of the app would work better as it can then be changed globally. 
class VapInstance():
    def __init__(self, name, host_name, icon_class, warnings, status, ip_address, display_name='', disabled = ''):
        self.name, self.host_name, self.icon_class, self.warnings, self.status, self.ip_address = name, host_name, icon_class, warnings, status, ip_address
        if not display_name : 
          self.display_name = self.name.split('-')
          if len(self.display_name)>1 : self.display_name = self.display_name[1:]
          else: self.display_name = self.display_name[0]
          self.display_name = ''.join(self.display_name).title()
        else : self.display_name = display_name
        self.disabled = disabled

    def __repr__(self):
        return 'Instance : ' + self.name + ' with hostname ' + self.host_name + '\n Status : ' + self.status + '\n'


class ProjectUsageCsvRenderer(csvbase.BaseCsvResponse):

    columns = [_("Instance Name"), _("VCPUs"), _("RAM (MB)"),
               _("Disk (GB)"), _("Usage (Hours)"),
               _("Time since created (Seconds)"), _("State")]

    def get_row_data(self):

        for inst in self.context['usage'].get_instances():
            yield (inst['name'],
                   inst['vcpus'],
                   inst['memory_mb'],
                   inst['local_gb'],
                   floatformat(inst['hours'], 2),
                   inst['uptime'],
                   capfirst(inst['state']))


class ProjectOverview(usage.UsageView):
    table_class = usage.ProjectUsageTable
    usage_class = usage.ProjectUsage
    template_name = 'project/myoverview/usage.html'
    csv_response_class = ProjectUsageCsvRenderer

    def get_data(self):
        super(ProjectOverview, self).get_data()
        return self.usage.get_instances()
 
    def error_message(self, error_type):
        return {  '403' : "You don't have permission to view that page. ", 
                  '404' : "You have tried to access a non-existent page - most probably a mistyped URL. If you got this error from clicking on a link, contact your administrator. ",
                  '500' : "You received a HTTP 500 error. This often happens if there has been an error with the salt stack. Contact your administrator for assistance. ",
                  'salt_error' : "There was an error connecting to the salt master. Contact your administrator for assistance. "}.get(error_type) or 'Unknown error. Please contact your administrator. '

    @salt_utils.timeout_function
    def get_instances(self):
        instances_list, more = openstack_api.nova.server_list(self.request)
        errors = []
        icinga_data = []
        icinga_data = salt_utils.get_icinga2_data(self.request)
        if not icinga_data : errors.append(self.error_message('salt_error'))
        if self.request.GET.get('error') : 
            errors.append(self.error_message(self.request.GET['error']))
            error_msg = 'There were errors attempting to access a page.</br>' + '</br>'.join(errors)
            messages.error(self.request, mark_safe(error_msg))

        instances_data = [('va-monitoring', 'va-monitoring',  'fa-heartbeat'), 
                            ('va-directory','va-directory', 'fa-group'), 
                            ('va-backup','va-backup', 'fa-database'),
                            ('va-owncloud','va-owncloud', 'fa-cloud'), 
                            ('va-fileshare','va-fileshare', 'fa-folder-open'),
                            ('va-email','va-email', 'fa-envelope'),]

        instances_dependent_on_directory = ['va-owncloud', 'va-fileshare', 'va-email']

        instances = []
        for instance in instances_data:
            instance = VapInstance(instance[0], instance[1], instance[2], 'n/a', 'not found', 'n/a')
            if instance.name in instances_dependent_on_directory and 'va-directory' not in [x.name for x in instances_list] : 
                instance.disabled = 'disabled'
            if instance.host_name in [x.name for x in instances_list]:
                if icinga_data : 
                    warnings = salt_utils.get_warnings_from_dict(icinga_data, instance.host_name)
                    if warnings : 
                        instance.warnings = warnings
                wanted_instance = [x for x in instances_list if x.name == instance.host_name]
                if wanted_instance : 
                    wanted_instance = wanted_instance[0]
                instance.status = {'ACTIVE':'running', 'SUSPENDED':'suspended', 'SHUTOFF' : 'not running'}.get(wanted_instance.status)

                ip_type = wanted_instance.addresses.keys()[0]
                if 'vmnet' in wanted_instance.addresses:
                    ip_type = 'vmnet'
                instance.ip_address = wanted_instance.addresses.get(ip_type)[0].get('addr')


            instances.append(instance)

        return instances

    def get_context_data(self, **kwargs):
      context = super(ProjectOverview, self).get_context_data(**kwargs)
      try: 
          instances = self.get_instances()
      except Exception: 
          context['vapourapps'] = []
          messages.error(self.request, 'There was an error trying to get minion data. Try again or contact your administrator. ')
          return context

      context['vapourapps'] = []
      for instance in instances:
          context['vapourapps'].append({'name': instance.display_name, 'icon_class': instance.icon_class, 'warnings' : instance.warnings, 'status' : instance.status, 'ip_address' : instance.ip_address, 'disabled' : instance.disabled})
#      context['view'].request.horizon['dashboard'] = apps_Dashboard
      return context


class WarningView(views.HorizonTemplateView):
    template_name = "private_cloud/_warning.html"
