import requests, json

class SaltAPI(object):
    def __init__(self, host):
        self.host = host
        self.token = None

    def login(self, username, password):
        req_data = {
             'username': username,
             'password': password,
             'eauth': 'keystone'
        }

        req = requests.post('%s/login' % self.host, data=req_data)
        for params in req.json()['return']:
            if 'token' in params:
                self.token = params['token']
                return True
        return False

    def call_function(self, target, func_name, func_args={}):
        """
        Calls a function from Salt execution module. Returns function results from all targets.
        The function arguments must be keyword-based, positioned ones are not allowed.
        """
        req_data = {
            'tgt': target,
            'fun': func_name,
            'kwarg': func_args,
            'client': 'local',
            'expr_form': 'grain'
        }
        req = requests.post(self.host, data=json.dumps(req_data), \
                            headers={'Content-Type': 'application/json', 'X-Auth-Token': self.token})

        targets = req.json()['return'] # This looks like: [{'target name': 'result of target'}, ...]
        # Let's convert it to sane form: [{'name': 'target name', 'result': 'result of target'}, ...]
        formatted_targets = []
        for target in targets:
            if len(target.keys()) != 1 or len(target.values()) != 1:
                continue
            target_name = target.keys()[0]
            target_result = target.values()[0]
            if target_result != 'None':
                formatted_targets.append({'name': target_name, 'result': target_result})
            else:
                formatted_targets.append({'name': target_name, 'result': None})
        return formatted_targets

    def call_function_single_target(self, target, func_name, func_args={}):
        """Same as call_function(), but returns the function result from one target"""
        req_json = self.call_function(target, func_name, func_args)
        if len(req_json) != 1:
            raise RuntimeError("The function was supposed to be called on one target, instead it was on %d" % len(req_json))
        return req_json[0]['result']

    def get_minions(self):
        req = requests.post(self.host + '/minions')
