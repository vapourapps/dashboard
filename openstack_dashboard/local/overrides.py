from django.utils.translation import ugettext_lazy as _

import horizon

project = horizon.get_dashboard('project')
project.name = _('Private cloud')
project.default_panel = 'myoverview'

compute = project.get_panel_group('compute')
"""This is an awful hack to make myoverview a replacement for overview"""
if 'myoverview' in compute.panels:
    # If it's loaded, move "myoverview" to the front of the list and unregister "overview" (their one)
    other_panels = [p for p in compute.panels if p != 'myoverview']
    compute.panels = ['myoverview'] + other_panels
    project.unregister(project.get_panel('overview').__class__)

admin = horizon.get_dashboard("admin")
admin.name = _("OpenStack Admin")

identity = horizon.get_dashboard("identity")
identity.name = _("Tenants and users")
