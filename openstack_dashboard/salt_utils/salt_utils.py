import re, ast, datetime, json, time, subprocess, stopit, os
from multiprocessing import Process
from openstack_dashboard import api as openstack_api
from django.core.cache import cache
import utils
from collections import Counter
from api import cinder, nova

class HostData():
    hostname = ''
    fqdn = ''
    address = ''
    def __init__(self, hostname, fqdn, address):
        self.hostname = hostname
        self.fqdn = fqdn
        self.address = address

#This is a decorator that goes before all functions that we want to time manage. 
def timeout_function(func, t_out = 60):
    def timeout(*args, **kwargs):
        if len(args) == 1 : 
            args = args[0]
        with stopit.ThreadingTimeout(t_out) as to_ctx_mgr:
            assert to_ctx_mgr.state == to_ctx_mgr.EXECUTING
            if kwargs: 
                result = func(args, kwargs)
            else: 
                if type(args) in [list, tuple, dict]: 
                 result = func(*args)
                else:
                  result = func(args)
        if to_ctx_mgr.state == to_ctx_mgr.TIMED_OUT:
            raise RuntimeError("Salt function took too long to execute. ")
        else: 
            return result
    return timeout

def salt_cache_value(key, value):
    cache.set(key, value, 60*60*2)

def salt_cache_get(key):
   return cache.get(key)


def salt_function_cache_result(key, func, args, kwargs = {}):
    if kwargs:
        result = func(*args, **kwargs)
    else: 
        result = func(*args)
    salt_cache_value(key, result)

#This is a decorator that will cache values for specific panels. 
#It will look for values in the cache; if they exist, use them and refill the cache asynchronously. Otherwise, get fresh data and fill the cache. 
@timeout_function
def salt_cache(key):
    def salt_cache_real(func):
        def cache_value(*args, **kwargs):
            a = cache.get(key) 
            if a:
               if kwargs: 
                    p = Process(target = salt_function_cache_result, args = (key, func, args, kwargs))
               else: 
                    p = Process(target = salt_function_cache_result, args = (key, func, args, kwargs))
               p.start()
               return a
            else: 
                if kwargs:
                    result = func(*args, **kwargs)
                else:
                    result = func(*args)
                salt_cache_value(key, result)
                return result
        return cache_value
    return salt_cache_real

#This is a decorator that when placed before a function will cause it to clear the value for the key. 
#It's use is to clear cache after adding new users. This will basically prompt the dashboard to reload the cache with fresh data and so the user won't have to refresh the page before seeing certain results. 
def salt_clear_key(key):
    def salt_clear_real(func):
        def clear_key(*args, **kwargs):
            cache.set(key, None)
            if kwargs:
                result = func(*args, **kwargs)
            else:
                result = func(*args)
            return result
        return clear_key
    return salt_clear_real
       
#---------- Some variables ----------#
backup_minion = 'G@role:backup' 
monitoring_minion = 'G@role:monitoring'
owncloud_minion = 'G@role:storage'
directory_minion = 'G@role:directory'
fileshare_minion = 'G@role:fileshare'
#---------- Functions using storage / owncloud ----------#

#Gets all files shared from owncloud
@salt_cache('owncloud_files')
def get_owncloud_files(request):
    api = utils.get_salt_api(request)
    files = api.call_function_single_target(owncloud_minion, 'storage.owncloud_shares')
    if files : files =  [[row['path'], row['share_with_displayname'], datetime.datetime.fromtimestamp(row.get('stime'))] for row in files]
    else : files = [[]]
    return files

@salt_cache('owncloud_users')
#Gets all users that share files on owncloud
def get_owncloud_users(request):
    api = utils.get_salt_api(request)
    users = api.call_function_single_target(owncloud_minion, 'storage.owncloud_users')
    if users : users = [[row['displayname'], 'Used : ' + str(row['quota'].get('used') ) + ' Total : ' + str(row['quota']['total']) + ' (' + str(row['quota']['relative'] * 100) + '%)', row['enabled']] for row in users if row['quota']]
    else : users = [[]]
    return users


#---------- Functions using directory / samba ----------#

samba_secondary_groups = ['Allowed RODC Password Replication Group', 'Enterprise Read-Only Domain Controllers', 'Denied RODC Password Replication Group', 'Pre-Windows 2000 Compatible Access', 'Windows Authorization Access Group', 'Certificate Service DCOM Access', 'Network Configuration Operators', 'Terminal Server License Servers', 'Incoming Forest Trust Builders', 'Read-Only Domain Controllers', 'Group Policy Creator Owners', 'Performance Monitor Users', 'Cryptographic Operators', 'Distributed COM Users', 'Performance Log Users', 'Account Operators', 'Event Log Readers', 'RAS and IAS Servers', 'Backup Operators', 'Domain Controllers', 'Server Operators', 'Enterprise Admins', 'Print Operators', 'Domain Computers', 'Cert Publishers', 'DnsUpdateProxy', 'Domain Guests', 'Schema Admins', 'Domain Users', 'Replicator', 'IIS_IUSRS', 'DnsAdmins', 'Guests', 'Users', 'TelnetClients', 'Administrators', 'HelpServicesGroup']

# DNS entry methods #
def manage_dns_entry(request, data, action = 'add'):
    api = utils.get_salt_api(request)
    entry_type = data['entry_type']
    name = data['name'] if data['name'] else '@'
    if entry_type in ['A', 'AAAA']:
      data = {'address' : data['address']}
    elif entry_type == 'MX':
      data = {'hostname' : data['hostname'], 'priority' : data['priority']}
    elif entry_type in ['NS', 'CNAME']:
      data = {'hostname' : data['hostname']}
    result = api.call_function_single_target(directory_minion,'samba.manage_dns_entry',{'name': name, 'entry_type': entry_type, 'data' : data, 'action' : action})
    return 'Record added successfully' in result or 'Record deleted successfuly' in result

def update_dns_entry(request, old_data, new_data):
    api = utils.get_salt_api(request)
    name = old_data['name'] if old_data['name'] else '@'
    entry_type = old_data['entry_type']
    result = api.call_function_single_target(directory_minion, 'samba.update_dns_entry', {'name': name, 'entry_type' : entry_type, 'old_data':old_data, 'new_data':new_data})
    return 'Record updated successfully' in result


# User methods #
@salt_clear_key('samba_users')
def manage_user_groups(request, user, add_to_groups, remove_from_groups):
    api = utils.get_salt_api(request)
    s = api.call_function_single_target(directory_minion, 'samba.update_user_groups', {'user':user, 'add_to_groups':add_to_groups, 'remove_from_groups':remove_from_groups})
    return s

@salt_clear_key('samba_users')
def add_activedir_user(request, user_data):
    api = utils.get_salt_api(request)
    if user_data.get('vpn'):
        openvpn_result = add_openvpn_user(request, user_data.get('username'))
        if not openvpn_result : return False
    samba_result = api.call_function_single_target(directory_minion, 'samba.add_user', user_data)
    return samba_result == None

@salt_clear_key('samba_users')
def delete_activedir_user(request, obj_id):
    api = utils.get_salt_api(request)
    result = api.call_function_single_target(directory_minion, 'samba.delete_user', {'username': obj_id})
    return result is None


#Gets all users from the samba server. The fields variable is a list for all the fields to retriev from user data. 
@salt_cache('samba_users')
def get_samba_users(request, fields = [], aliases = False, get_groups = True, get_samba_log = True):
    api = utils.get_salt_api(request)
    result = api.call_function_single_target(directory_minion, 'samba.list_users', {'get_groups' : get_groups, 'get_samba_log' : get_samba_log})
    if type(result) == str: return [[]]
    if not get_groups and not get_samba_log: 
        if not fields : return result
        return [[user[field] for field in fields] for user in result['users']]

    return result


def sync_cache(request):
    api = utils.get_salt_api(request)
    api.call_function_single_target(directory_minion, 'samba.purge_cache')
    api.call_function_single_target(directory_minion, 'samba.build_cache')

@salt_cache('samba_users_log')
def get_samba_log(request):
    api = utils.get_salt_api(request)
    try:
        result = api.call_function_single_target(directory_minion, 'samba.users_log')
    except Exception as e: 
        result = {'info': {'username' : ''}}
    return result

@salt_cache('samba_users_last_logins')
def users_last_logins(request):
    api = utils.get_salt_api(request)
    result = api.call_function_single_target(directory_minion, 'samba.users_last_logins')
    return result

@salt_cache('openvpn_users')
def get_openvpn_users(request, get_status = True, list_samba_users = True):
    api = utils.get_salt_api(request)
    users = api.call_function_single_target(directory_minion, 'openvpn.list_users', {'get_user_status' : get_status, 'list_samba_users' : list_samba_users})
    if type(users) != dict: return False
    return users

@salt_clear_key('openvpn_users')
def add_openvpn_user(request, username):
    api = utils.get_salt_api(request)
    success = api.call_function_single_target(directory_minion, 'openvpn.add_user', {'username': username})
    return success

def list_user_logins(request, user):
    api = utils.get_salt_api(request)
    return api.call_function_single_target(directory_minion, 'openvpn.list_user_logins', {'user' : user})

@salt_clear_key('openvpn_users')
def revoke_openvpn_user(request, username):
    api = utils.get_salt_api(request)
    success = api.call_function_single_target(directory_minion, 'openvpn.revoke_user', {'username': username})
    return success

def get_user_cert(request, user):
    api = utils.get_salt_api(request)
    return api.call_function_single_target(directory_minion, 'openvpn.get_config', {'username' : user})

# Group methods #
def get_groups_with_users(request, group_names = []):
    api = utils.get_salt_api(request)
    groups = api.call_function_single_target(directory_minion, 'samba.get_groups_with_users', {'group_names':group_names})
    if type(groups) == str : groups = [[]]
    return groups

def change_password(request, username, new_pass, again_at_login = False):
    api = utils.get_salt_api(request)
    success = api.call_function_single_target(directory_minion, 'samba.change_password', {'username' : username, 'password' : new_pass, 'again_at_login' : again_at_login})
    return success

def new_group(request, group_name):
    api = utils.get_salt_api(request)
    add_group = api.call_function_single_target(directory_minion, 'samba.add_group', {'name' : group_name})
    return add_group == None

def delete_group(request, group_name):
    api = utils.get_salt_api(request)
    result = api.call_function_single_target(directory_minion, 'samba.samba_tool', {'args':['group', 'delete', group_name]})
    return result

@salt_clear_key('samba_users')
def add_user_to_group(request, users, group_name):
    api = utils.get_salt_api(request)
    result = api.call_function_single_target(directory_minion, 'samba.manage_users_in_groups', {'users_groups':{user:[group_name] for user in users}})
    return result

def remove_user_from_group(request, user, group_name):
    api = utils.get_salt_api(request)
    result = api.call_function_single_target(directory_minion, 'samba.samba_tool', {'args':['group', 'removemembers', group_name, user]})
    return 'Removed members from group' in result

def get_vpn_status(request):
    api = utils.get_salt_api(request)
    status = api.call_function_single_target(directory_minion, 'openvpn.get_status')
    if type(status) != dict : return {}
    return status

#---------- Misc functions ----------#

#Uses nmap to scan for computers in the network. Used to see if a certain computer is in the network but not connected to salt master.
#Not actually being used right now, but it was an idea for some services. 
#def get_network_computers(addresses_to_scan):
#    nm = nmap.PortScanner()
#    hosts = []
#    for address in addresses_to_scan:
#        nm.scan(hosts = address, arguments = '-sP -n -sn')
#        hosts += nm._scan_result['scan'].keys()
#    return hosts


#Gets the fqdn for the specified hosts. 
@salt_cache('host_fqdn_pairs')
def get_host_fqdn_pairs(request):
    api = utils.get_salt_api(request)
    rows = api.call_function_single_target(monitoring_minion, 'mine.get', {'tgt' : '*', 'fun' : 'inventory'})
    return { host : rows[host]['fqdn'] for host in rows }


#Gets the fqdn for the specified host. 
def get_host_fqdn(request, host):
    api = utils.get_salt_api(request)
    fqdn = api.call_function_single_target(host, 'grains.get', {'key' : 'fqdn'})
    return fqdn


#---------- Functions using backup ----------#
#Gets all backed up files for the host. 
@salt_cache('backuppc_data')
def get_backed_up_files(request, hosts_fqdns):
    api = utils.get_salt_api(request)
    rows = api.call_function_single_target(backup_minion, 'backuppc.list_folders', {'hostnames' : hosts_fqdns.values()})
    rows = {host : rows[hosts_fqdns[host]] for host in hosts_fqdns}
    return rows

#Get the last backups for all the hosts_fqdns  listed.
@salt_cache('backuppc_last_backups')
def get_last_backups(request, hosts_fqdns):
    api = utils.get_salt_api(request)
    #The last backups are received in the form of a perl variable, which needs to be parsed to a python dict. 
    last_backups = api.call_function_single_target(backup_minion, 'cmd.run', {'cmd' : '/usr/share/backuppc/bin/BackupPC_serverMesg status hosts', 'runas' : 'backuppc', 'shell' : '/bin/bash', 'cwd' : '/usr/share/backuppc/bin'})
    regex_get_vals = '\%Status = \((.*)\).*;'
    last_backups = '{' + re.sub(regex_get_vals, r'\1', last_backups) + '}'
    last_backups = last_backups.replace('=>', ':').replace('undef', '\"undef\"').replace('Got reply:', '')
    #Have to escape because paths can sometimes have extra backlashes.
    last_backups = json.loads(last_backups.encode('unicode-escape')) 
    return {hosts_fqdns[host] : last_backups.get(host).get('lastGoodBackupTime', 'n/a') for host in hosts_fqdns if host in last_backups}

#Function which sets how long backups should be kept for. 
def keep_backups(request, no_days):
    api = utils.get_salt_api(request)
    changes_made = api.call_function_single_target(backup_minion, 'backuppc.keep_backups', {'no_days' : no_days})
    return changes_made

@salt_clear_key('backuppc_data')
def add_new_backup(request, host_fqdn, new_backup_file):
    api = utils.get_salt_api(request)
    if new_backup_file[0] != '/' : 
        new_backup_file = '/' + new_backup_file
    result = api.call_function_single_target(backup_minion, 'backuppc.add_folder', {'hostname' : host_fqdn, 'folder' : new_backup_file})
    return result

#Calls a function from saltstack to remove a file from its respective backup list. Returns whether the file exists or not. (TODO maybe check for other errors?)
#Example usage : success = salt_api.remove_backup(self.request, 'va-backup', '/some/file')
#                if not success: 
#                   #print error
@salt_clear_key('backuppc_data')
def remove_backup(request, backup_host, backup_file):
    api = utils.get_salt_api(request)
    if backup_file[0] != '/' : backup_file = '/' + backup_file
    file_exists = api.call_function_single_target(backup_minion, 'backuppc.rm_folder', {'hostname':backup_host, 'folder':backup_file})
    return 'has been deleted from backup list' in file_exists


def add_default_backups(request, hosts):
    api = utils.get_salt_api(request)
    result = api.call_function_single_target(backup_minion, 'backuppc.add_default_paths', {'hosts' : hosts})
    return result

#---------- Functions using fileshare ----------#

@salt_cache('fileshare')
def get_fileshares(request):
    api = utils.get_salt_api(request)
    result = api.call_function_single_target(fileshare_minion, 'fileshare.get_all_fileshares')
    return result

#---------- Functions using monitoring ----------#

def get_domain_and_company_name(request):
    api = utils.get_salt_api(request)
    result = api.call_function_single_target(directory_minion, 'pillar.items')
    return result.get('domain'), result.get('company')

def create_instance(request, instance_name, instance_profile):
    if 'directory' not in instance_name: 
        new_volume = cinder.volume_create(request = request, size = request.POST['volume_size'], name = instance_name, description = 'Volume created for instance ' + instance_name + ' and profile ', volume_type = '')

        # when a volume is created, it's status isn't immediately available.
        # so we wait until it is, and then attach it to the instance. 
        # this may take some time so that's why we're doing it in a separate process. 
        c = 0
        while cinder.volume_get(request, new_volume.id).status != 'available' and c < 40: 
            time.sleep(5)
            c += 1

    api = utils.get_salt_api(request)

    result = api.call_function_single_target(monitoring_minion, 'cloud.profile', {'names' : [instance_name], 'profile' : instance_profile})
    

    print 'Result is : ', result
    instance_id = result[instance_name]['id']
    print 'Id is : ', instance_id

    if 'directory' not in instance_name:
        print 'Attempting to attach.'
        result = nova.instance_volume_attach(request = request, volume_id = new_volume.id , instance_id = instance_id, device = '')
        print 'Result is : ', result
        


def enable_instance(request, instance_name):
    instance_profile = instance_name = {
      'email' : 'va-email',
      'owncloud' : 'va-owncloud',
      'projects' : 'va-projects',
      'fileshare' : 'va-fileshare', 
      'backup' : 'va-backup',
      'directory' : 'va-directory',
    }[instance_name]
    p = Process(target = create_instance, args = [request, instance_name, instance_profile], kwargs = {})
    p.start()



@salt_cache('data_inventory')
def get_data_inventory(request):
    api = utils.get_salt_api(request)
    hosts_info = api.call_function_single_target(monitoring_minion, 'mine.get', {'tgt' : '*', 'fun' : 'inventory'})
    return hosts_info

#Function which should get all salt data from monitoring and return the required fields. 
@salt_cache('inventory')
def get_table_rows(request, columns):
    api = utils.get_salt_api(request)
    hosts = get_hosts(request)
    rows = []
    hosts_info = api.call_function_single_target(monitoring_minion, 'mine.get', {'tgt' : '*', 'fun' : 'inventory'})
    for host in hosts_info:
        rows_host = []
        for column in columns: 
            column = column.split(' ')
            col_vals = ''
            for att in column : 
                new_att = hosts_info.get(host).get(att)
                if new_att : 
                    if isinstance(new_att, dict):
                        s = ''
                        for key in new_att:
                          s += str(key) + ' = ' + str(new_att[key][0]) + ' '
                        col_vals += s
                    else:
                        col_vals += str(new_att) + ' '
            rows_host += [col_vals]
        if rows_host:
            rows = rows + [rows_host]
    return rows



#Gets all hosts from monitoring. 
@salt_cache('monitoring_hosts')
def get_hosts(request):
    api = utils.get_salt_api(request)
    hosts = json.loads(api.call_function_single_target(monitoring_minion, 'cmd.run', {'cmd' : 'salt-run manage.status --out json'}))['up']
    return hosts


def get_instance_status(request, instance_name):
    try : 
        instances_list, more = openstack_api.nova.server_list(request)
        if instance_name in [x.name for x in instances_list]:
            wanted_instance = [x for x in instances_list if x.name == instance_name][0]
        else: 
            return None
        return {'ACTIVE':'running', 'SUSPENDED':'suspended', 'SHUTOFF' : 'not running'}.get(wanted_instance.status)
    except Exception as e: 
        return False




#Gets all host data from monitoring. May need to be changed to allow from multiple hosts.
@salt_cache('icinga2_data')
def get_icinga2_data(request):
    try :
        api = utils.get_salt_api(request)
        result = api.call_function_single_target(monitoring_minion, 'monitoring.icinga2')
        return result
    except Exception as e:
        return False

#Give it a dictionary of the data from icinga2 and it will return a dictionary containing warnings and critical errors. 
def get_warnings_from_dict(data, host_name = ''):
    if type(data) != list:
        return False

    status_list = {x['host_name']: Counter([y['state'] for y in x['services']]) for x in data}
    for key in status_list:
        if 1 in status_list[key]:
            if 2 in status_list[key]:
                status_list[key] = str(status_list[key][1]) + ' warnings ' + str(status_list[key][2]) + ' critical'
            else:
                status_list[key] = str(status_list[key][1]) + ' warnings'
        elif 2 in status_list[key]:
            status_list[key] = str(status_list[key][2]) + ' critical'
        else:
            status_list[key] = 'None'
    if host_name == '':
        return status_list
    else:
        if not any([host_name in x for x in status_list]):
            return 'n/a'
        else:
            status = status_list.get(host_name)
            if not status : status = status_list[[x for x in status_list if host_name in x][0]]
            return status
